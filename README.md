# DATASET

Use the following url style to import files

https://bitbucket.org/hanxiao/dataset/raw/master/connected4.graphml


ckisemicomplete.graphml : critical kernel imperfect semi-complete digraphs within 10 vertices;
or minimal cliques without kernels within 10 vertices;
or obstructions for every clique having a kernel within 10 vertices.
construction: starting with a directed cycle and connect nonadjacent vertices by a pair of opposite arcs.

obst4linemulti.graphml : a full list of obstructions for line multi-graphs

obst4perfect.graphml : obstructions for perfect graphs within 10 vertices.
construction: odd hole(chordless odd cycle larger than or equal to 5) and aniti-hole(complete of an odd hole).